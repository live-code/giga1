import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(f => f.CatalogModule) },
  { path: 'homepage', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
  {
    path: 'contacts',
    loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule)
  },
  { path: '', redirectTo: 'homepage', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'demos', loadChildren: () => import('./features/demos/demos.module').then(m => m.DemosModule) },
  { path: 'forms', loadChildren: () => import('./features/forms/forms.module').then(m => m.FormsModule) }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
