import { Component } from '@angular/core';

interface User {
  id: number
}
@Component({
  selector: 'gig-root',
  template: `
    <gig-navbar></gig-navbar>
    <hr>
    
    <div class="m-2">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [`
   
  `]
})
export class AppComponent {
  doSomething(a: number) {

  }

}
