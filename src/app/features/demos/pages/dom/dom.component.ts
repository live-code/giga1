import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PanelComponent } from '../../../../shared/components/panel/panel.component';

@Component({
  selector: 'gig-dom',
  template: `
      <h1>DOM Examples</h1>
      <input #input class="border">
      <button (click)="save()">CLICK ME</button>
     
      <gig-panel title="Pippo" icon="fa fa-google" (iconClick)="openUrl('http://www.fabiobiondi.dev')">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga ipsum maxime quas rerum temporibus! Illo quia, voluptatem? Deleniti dolorem ipsam officia. Ab ducimus eaque et hic illum libero natus quo.  
      </gig-panel>
  
      <div class="m-4"></div>
      
      <gig-panel title="Pluto" icon="fa fa-facebook" (iconClick)="doSomething()">
        <button class="btn" (click)="oneClick()">1</button>
        <button class="btn">2</button>
        <button class="btn">3</button>
      </gig-panel>
      
  `,
  styles: [
  ]
})
export class DomComponent implements OnInit, AfterViewInit {
  @ViewChild('input') myInput!: ElementRef<HTMLInputElement>
  @ViewChild(PanelComponent) myPanel!: PanelComponent;

  constructor() {
    console.log('ctr', this.myPanel)
  }

  ngOnInit(): void {
    console.log('ngOnInit', this.myPanel)
  }
  ngAfterViewInit(): void {
    console.log('ngAfterVIewINit', this.myPanel)
  }
  save() {
    this.myInput.nativeElement.value = 'ciao'
  }


  oneClick() {
    console.log('ciao')
  }

  openUrl(url: string) {
    window.open(url)
  }
  doSomething() {
    console.log('do something')
  }
}
