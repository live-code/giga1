import { Component, OnInit } from '@angular/core';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'gig-lifecycle',
  template: `
    
    <gig-chartjs2 [options]="cfg"></gig-chartjs2>
    <gig-chartjs [options]="cfg"></gig-chartjs>
    <button class="btn" (click)="changeChartConfig()">Update CFG</button>
    
    <h1>temperature</h1>
    <gig-chartjs-temperatures
      (barClick)="doSomething($event)"
      [temps]="data"></gig-chartjs-temperatures>
    
    <gig-chartjs2></gig-chartjs2>
    <button class="btn" (click)="data = [6, 5, 3, 2, 1, 1]">Update DAta</button>
    <hr>
    <gig-weather2  [color]="myColor" [city]="myCity"></gig-weather2>
    <gig-weather [color]="myColor" [city]="myCity"></gig-weather>
    <hr>
    <button class="btn" (click)="myColor = 'cyan'">cyan</button>
    <button class="btn" (click)="myColor = 'orange'">orange</button>
    <button class="btn" (click)="myCity = 'Trieste'">Trieste</button>
    <button class="btn" (click)="myCity = 'Palermo'">Palermo</button>
  `,
  styles: [
  ]
})
export class LifecycleComponent {
  myCity: string = 'MIlano';
  myColor: string = 'red';
  data = [10, 20, 30, 40, 50, 60];
  cfg: ChartConfiguration = {
    type: 'line',
    data: {
      labels: ['Purple', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [{
        label: '# of Votes',
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  };

  changeChartConfig() {
    this.cfg = {
      type: 'bar',
      data: {
        labels: ['Purple', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [33, 29, 33, 35, 32, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        },
        onClick: (evt: any, el: any, chart: any): any => {
          console.log(evt)
          //console.log('Active el list: ', chart.data.datasets[el[0].datasetIndex].label, chart.data.datasets[el[0].datasetIndex].data[el[0].index])
/*
          let point = chart.getElementsAtEventForMode(evt, 'point', {
            intersect: true
          }, true);

          console.log('Mode point list: ', chart.data.datasets[point[0].datasetIndex].label, chart.data.datasets[point[0].datasetIndex].data[point[0].index])*/
        }
      },

    };

  }

  doSomething(evt: any) {
    console.log(evt)
    //window.alert('cclicked bar')
  }
}
