import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { LifecycleRoutingModule } from './lifecycle-routing.module';
import { LifecycleComponent } from './lifecycle.component';


@NgModule({
  declarations: [
    LifecycleComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LifecycleRoutingModule
  ]
})
export class LifecycleModule { }
