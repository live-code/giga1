import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directives1Component } from './directives1.component';

const routes: Routes = [{ path: '', component: Directives1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directives1RoutingModule { }
