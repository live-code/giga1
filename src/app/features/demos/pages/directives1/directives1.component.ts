import { Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../../../core/auth.service';
import { PanelComponent } from '../../../../shared/components/panel/panel.component';

@Component({
  selector: 'gig-directives1',
  template: `
    
    <div gigIfLogged >ADMIN 1 FEATURES</div>
    <div *gigIfSignIn="'admin'" >ADMIN 2 FEATURES</div>
    <button class="btn" gigUrl="http://www.google.com">google</button>
    <div [gigTooltip]="[10, 20, 30]" [gigPad]="val">A</div>
    
    <div [gigMargin]="val">B</div>
    <i class="fa fa-plus-circle" (click)="val = val+1"></i>
    
  `,
})
export class Directives1Component  {
  val = 0;
}
