import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { Directives1RoutingModule } from './directives1-routing.module';
import { Directives1Component } from './directives1.component';


@NgModule({
  declarations: [
    Directives1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    Directives1RoutingModule
  ]
})
export class Directives1Module { }
