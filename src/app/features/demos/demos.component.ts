import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gig-demos',
  template: `
    <button routerLink="dom" class="btn" routerLinkActive="!bg-pink-300">dom</button>
    <button routerLink="lifecycle" class="btn" routerLinkActive="!bg-pink-300">lifecycle</button>
    <button routerLink="directive" class="btn" routerLinkActive="!bg-pink-300">directive</button>

    <hr class="container mt-4 mb-4">
    <router-outlet></router-outlet>
  `,
})
export class DemosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
