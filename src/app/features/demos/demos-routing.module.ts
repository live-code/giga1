import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemosComponent } from './demos.component';

const routes: Routes = [
  {
    path: '', component: DemosComponent,
    children: [
      { path: 'lifecycle', loadChildren: () => import('./pages/lifecycle/lifecycle.module').then(m => m.LifecycleModule) },
      { path: 'dom', loadChildren: () => import('./pages/dom/dom.module').then(m => m.DomModule) },
      { path: 'directive', loadChildren: () => import('./pages/directives1/directives1.module').then(m => m.Directives1Module) },
      { path: '', redirectTo: 'lifecycle', pathMatch: 'full'}
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemosRoutingModule { }


