import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { forkJoin, map, switchMap, toArray } from 'rxjs';
import { User } from '../../model/user';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'gig-catalog',
  template: `
    <h1>Catalog: {{catalogService.users.length}} items</h1>
    
    <div *ngIf="catalogService.error">Ahia!</div>
    
    <gig-catalog-add
      [data]="catalogService.users"
      (add)="catalogService.addUserHandler($event)"></gig-catalog-add>
    
    <gig-catalog-list 
      [data]="catalogService.users"
      (deleteUser)="catalogService.deleteUserHandler($event)"
    ></gig-catalog-list>
  `,
})
export class CatalogComponent {
  constructor(
    public catalogService: CatalogService,
  ) {
    this.catalogService.getAll()
  }
}
