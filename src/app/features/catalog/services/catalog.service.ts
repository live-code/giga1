import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../../model/user';

@Injectable()
export class CatalogService {
  users: Partial<User>[] = [];
  error: boolean = false;
  constructor(private http: HttpClient) {
  }

  getAll() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => {
        this.users = res
      })
  }

  deleteUserHandler(idToRemove: number): void {
    this.http.delete<User[]>('http://localhost:3000/users/' + idToRemove)
      .subscribe(() => {
        // const index = this.users.findIndex(u => u.id === idToRemove)
        // this.users.splice(index, 1)
        this.users = this.users.filter(u => u.id !== idToRemove)
      })

  }

  addUserHandler(formData: Pick<User, 'name' | 'email'>) {
    this.http.post<Pick<User, 'name' | 'email' | 'id'>>('http://localhost:3000/users/', formData)
      .subscribe((res) => {
        // this.users.push(res)
        this.users = [...this.users, res]
      })
  }
}
