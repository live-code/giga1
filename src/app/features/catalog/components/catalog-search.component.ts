import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'gig-catalog-add',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <pre>valid: {{f.valid}}</pre>
    <h1>{{data.length}} users</h1>
    <form #f="ngForm" (submit)="add.emit(f.value)">
      <input type="text" required ngModel name="name" class="border" placeholder="Your Name">
      <input type="text" required  ngModel name="email" class="border" placeholder="e-mail">
      <button type="submit" [disabled]="f.invalid">save</button>
    </form>
  `,
})
export class CatalogSearchComponent  {
  @ViewChild('f') form!: NgForm;
  @Input() data: any[] = []
  @Output() add = new EventEmitter<{ name: string, email: string}>()

  render() {
    console.log('render: catalog add')
  }

}
