import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'gig-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>list</h1>
    <div *ngFor="let user of data"  class="flex justify-between items-center">
      {{user.name}}
      <button class="btn" 
              (click)="deleteUser.emit(user.id)">del</button>
    </div>
  `,
})
export class CatalogListComponent  {
  @Input() data: Partial<User>[] | null = null
  @Output() deleteUser = new EventEmitter<number>()
}
