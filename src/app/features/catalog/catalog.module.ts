import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CatalogComponent } from './catalog.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogSearchComponent } from './components/catalog-search.component';
import { CatalogService } from './services/catalog.service';


const routes: Routes = [
  { path: '', component: CatalogComponent }
];

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogSearchComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    CatalogService
  ]
})
export class CatalogModule { }
