import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { LostPassComponent } from './pages/lost-pass.component';
import { RegistrationComponent } from './pages/registration.component';
import { SignInComponent } from './pages/sign-in.component';


const routes: Routes = [
  {
    path: '', component: LoginComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'lost', component: LostPassComponent },
      { path: 'signin', component: SignInComponent },
      { path: 'help', loadChildren: () => import('./pages/help/help.module').then(m => m.HelpModule) },
    ]
  },
 ];

@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
    LostPassComponent,
    RegistrationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class LoginModule { }
