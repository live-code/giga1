import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gig-login',
  template: `
    <h1>login</h1>
    
    <router-outlet></router-outlet>
 
    <hr>
    <div class="flex gap-x-2">
      <button class="btn" routerLink="registration">Regi</button>
      <button class="btn" routerLink="lost">Lost</button>
      <button class="btn" routerLink="signin">Sign</button>
      <button class="btn" routerLink="help">help</button>
    </div>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  page = 'signin'
  constructor() { }

  ngOnInit(): void {
  }

}
