import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CarouselComponent } from './components/carousel.component';
import { HomeComponent } from './home.component';
import { NewsComponent } from './components/news.component';
import { HeroComponent } from './components/hero.component';

@NgModule({
  declarations: [
    HomeComponent,
    NewsComponent,
    HeroComponent,
    CarouselComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent }
    ])
  ]
})
export class HomeModule { }
