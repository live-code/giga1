import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gig-home',
  template: `
    <p>
      home works!
    </p>
    <gig-news></gig-news>
    <gig-panel></gig-panel>
    <gig-datagrid></gig-datagrid>
    <input [ngModel]="label">
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  label = 'ciao'
  constructor() { }

  ngOnInit(): void {
  }

}
