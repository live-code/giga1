import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

interface Auth {
  token: string;
  role: 'admin' | 'moderator';
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth | null = null;
  // o------o---->
  auth$ = new BehaviorSubject<Auth | null>(null)

  login() {
    this.auth = { token: 'Bearer 1234', role: 'moderator'}
    this.auth$.next({ token: 'Bearer 1234', role: 'moderator'})
  }

  logout() {
    this.auth = null;
    this.auth$.next(null)
  }

  get isLogged(): boolean {
    return !!this.auth;
  }
}
