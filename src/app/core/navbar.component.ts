import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'gig-navbar',
  template: `
    <button routerLink="login" class="btn" routerLinkActive="!bg-pink-300">login</button>
    <button routerLink="homepage" class="btn" routerLinkActive="!bg-pink-300">Homepage</button>
    <button routerLink="catalog" class="btn" routerLinkActive="!bg-pink-300">Catalog</button>
    <button routerLink="demos" class="btn" routerLinkActive="!bg-pink-300">demos</button>
    <button [routerLink]="'contacts'" class="btn" routerLinkActive="!bg-pink-300">Contacts</button>
    <button class="btn" (click)="auth.login()">login</button>
    <button class="btn" gigIfLogged (click)="auth.logout()">logout</button>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

}
