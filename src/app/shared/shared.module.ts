import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DatagridModule } from './components/datagrid/datagrid.module';
import { PanelModule } from './components/panel/panel.module';
import { WeatherComponent } from './components/weather.component';
import { ChartjsComponent } from './components/chartjs.component';
import { ChartjsTemperaturesComponent } from './components/chartjs-temperatures.component';
import { Chartjs2Component } from './components/chartjs2.component';
import { Weather2Component } from './components/weather2.component';
import { BgDirective } from './components/directives/bg.directive';
import { MarginDirective } from './components/directives/margin.directive';
import { PadDirective } from './components/directives/pad.directive';
import { TooltipDirective } from './components/directives/tooltip.directive';
import { UrlDirective } from './components/directives/url.directive';
import { IfLoggedDirective } from './components/directives/if-logged.directive';
import { IfSignInDirective } from './components/directives/if-sign-in.directive';


@NgModule({
  declarations: [
    WeatherComponent,
    ChartjsComponent,
    ChartjsTemperaturesComponent,
    Chartjs2Component,
    Weather2Component,
    BgDirective,
    MarginDirective,
    PadDirective,
    TooltipDirective,
    UrlDirective,
    IfLoggedDirective,
    IfSignInDirective,
  ],
  imports: [
    CommonModule,
    PanelModule,
    FormsModule,
    DatagridModule
  ],
  exports: [
    DatagridModule,
    FormsModule,
    PanelModule,
    WeatherComponent,
    ChartjsComponent,
    ChartjsTemperaturesComponent,
    Chartjs2Component,
    Weather2Component,
    BgDirective,
    MarginDirective,
    PadDirective,
    TooltipDirective,
    UrlDirective,
    IfLoggedDirective,
    IfSignInDirective
  ]
})
export class SharedModule { }
