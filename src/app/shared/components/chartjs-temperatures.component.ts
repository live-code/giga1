import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'gig-chartjs-temperatures',
  template: `
    <gig-chartjs [options]="cfg"></gig-chartjs>
  `,
})
export class ChartjsTemperaturesComponent {
  @Input() temps: number[] = [];
  @Output() barClick = new EventEmitter()
  cfg: any;

  ngOnChanges(): void {
    console.log('change')
    const cloned: ChartConfiguration = { ...DEFAULT_OPTIONS }
    cloned.data.datasets[0].data = this.temps
    cloned.options = {
      ...DEFAULT_OPTIONS.options,
      onClick: (evt: any, el: any, chart: any): any => {
        this.barClick.emit(evt)
      }
    }
    this.cfg = cloned;
  }

  ngOnInit(): void {
    const cloned: ChartConfiguration = { ...DEFAULT_OPTIONS }
    cloned.data.datasets[0].data = this.temps
    cloned.options = {
      ...DEFAULT_OPTIONS.options,
      onClick: (evt: any, el: any, chart: any): any => {
        this.barClick.emit(evt)
      }
    }
    this.cfg = cloned;
  }

}


const DEFAULT_OPTIONS: ChartConfiguration = {
  type: 'bar',
  data: {
    labels: ['L', 'M', 'M', 'G', 'V', 'S'],
    datasets: [{
      label: 'temperatures',
      data: [33, 29, 33, 35, 32, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  }
};
