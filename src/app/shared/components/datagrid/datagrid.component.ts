import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gig-datagrid',
  template: `
    <p>
      datagrid works!
    </p>
    <gig-panel></gig-panel>
    <gig-datagrid-cell></gig-datagrid-cell>
    <gig-datagrid-row></gig-datagrid-row>
  `,
  styles: [
  ]
})
export class DatagridComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
