import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from '../panel/panel.module';
import { DatagridCellComponent } from './datagrid-cell.component';
import { DatagridRowComponent } from './datagrid-row.component';
import { DatagridComponent } from './datagrid.component';



@NgModule({
  declarations: [
    DatagridComponent,
    DatagridCellComponent,
    DatagridRowComponent
  ],
  imports: [
    CommonModule,
    PanelModule
  ],
  exports: [
    DatagridComponent
  ]
})
export class DatagridModule { }
