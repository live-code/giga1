import { AfterViewInit, Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart, ChartItem, registerables } from 'chart.js';
Chart.register(...registerables);

@Component({
  selector: 'gig-chartjs',
  template: `
    <canvas #host width="400" height="200"></canvas>
  `,
  styles: [
  ]
})
export class ChartjsComponent implements AfterViewInit {
  @ViewChild('host') host!: ElementRef<HTMLCanvasElement>
  @Input() options: any;
  myChart!: Chart;

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['options'].firstChange) {
      if (changes['options'].currentValue.type !== changes['options'].previousValue.type) {
        this.myChart.destroy();
        const ctx = (this.host.nativeElement)?.getContext('2d');
        this.myChart = new Chart(ctx!, this.options)
      } else {
        this.myChart.data = this.options.data;
        this.myChart.update()
      }
    }
  }


  ngAfterViewInit(): void {
    console.log('after view init')
    const ctx = (this.host.nativeElement)?.getContext('2d');
    this.myChart = new Chart(ctx!, this.options)
  }


}
