import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[gigMargin]'
})
export class MarginDirective {
  @Input() set gigMargin(val: number) {
    // this.el.nativeElement.style.margin = (val * 10) + 'px'
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      (val * 10) + 'px'
    )
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {}
/*

  ngOnChanges() {
    this.el.nativeElement.style.margin = (this.gigMargin * 10) + 'px'
  }
*/

}
