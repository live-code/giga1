import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../../../core/auth.service';

@Directive({
  selector: '[gigIfLogged]'
})
export class IfLoggedDirective {

  @HostBinding('style.display') get display() {
    return this.authSrv.isLogged ? null : 'none';
  }

  constructor(private authSrv: AuthService) {
  }

}
