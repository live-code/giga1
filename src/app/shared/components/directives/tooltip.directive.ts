import { Directive, ElementRef, HostListener, Input, ViewContainerRef } from '@angular/core';
import { ChartjsTemperaturesComponent } from '../chartjs-temperatures.component';
import { Chartjs2Component } from '../chartjs2.component';
import { PanelComponent } from '../panel/panel.component';

@Directive({
  selector: '[gigTooltip]'
})
export class TooltipDirective {
  @Input() gigTooltip: number[] = [];
  constructor(
    private el: ElementRef,
    private view: ViewContainerRef
  ) {

  }
  @HostListener('mouseover')
  over() {
    const ref = this.view.createComponent(ChartjsTemperaturesComponent)
    ref.instance.temps = this.gigTooltip;
    // ....
  }


  @HostListener('mouseout')
  out() {
    this.view.clear()
  }
}
