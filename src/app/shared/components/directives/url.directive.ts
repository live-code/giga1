import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[gigUrl]'
})
export class UrlDirective {
  @HostBinding('style.cursor') cursor = 'pointer'
  @Input() gigUrl: string = '';
  color: string | null = null;

  @HostBinding('style.color') get c() {
    return this.color
  }

  @HostListener('click')
  openUrl() {
    window.open(this.gigUrl)
  }

  @HostListener('mouseover')
  over() {
    this.color = 'red';
  }

  @HostListener('mouseout')
  out() {
    this.color = null;
  }
  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
  }
}
