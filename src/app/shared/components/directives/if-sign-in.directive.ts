import { Directive, HostBinding, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../../core/auth.service';

@Directive({
  selector: '[gigIfSignIn]'
})
export class IfSignInDirective {
  @Input() gigIfSignIn!: 'admin' | 'moderator';

  constructor(
    private authSrv: AuthService,
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>
  ) {
    this.authSrv.auth$
      .subscribe(auth => {
        if (!!auth && this.gigIfSignIn == auth.role) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })

  }

}
