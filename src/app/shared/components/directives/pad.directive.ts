import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[gigPad]'
})
export class PadDirective {
  @Input() gigPad: number = 0;

  @HostBinding('style.padding') get padding() {
    return (this.gigPad * 10) + 'px'
  }

}
