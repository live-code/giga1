import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'gig-panel',
  template: `
    <div class="border border-4 border-sky-500 rounded-2xl ">
      <h1
        (click)="opened = !opened"
        class="text-3xl bg-sky-500 p-2 text-white flex justify-between items-center">
        <div>Panel {{title}}</div>
        <i *ngIf="icon" [class]="icon" (click)="iconClick.emit()"></i>
      </h1>
      <div class="p-2" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class PanelComponent {
  @Input() title = '';
  @Input() icon = '';
  @Output() iconClick = new EventEmitter()
  opened = false;
}
