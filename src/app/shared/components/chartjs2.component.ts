import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'gig-chartjs2',
  template: `
    <canvas #host width="400" height="200"></canvas>
  `,
})
export class Chartjs2Component  {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLCanvasElement>
  @Input() options: any;
  myChart!: Chart;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['options'].firstChange) {
      this.init()
    } else {
      if (changes['options'].currentValue.type !== changes['options'].previousValue.type) {
        this.myChart.destroy();
        const ctx = (this.host.nativeElement)?.getContext('2d');
        this.myChart = new Chart(ctx!, this.options)
      } else {
        this.myChart.data = this.options.data;
        this.myChart.update()
      }
    }
  }

  init(): void {
    console.log('after view init')
    const ctx = (this.host.nativeElement)?.getContext('2d');
    this.myChart = new Chart(ctx!, this.options)
  }

}
